package com.mycompany.project_gambar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Connection con = null;
        PreparedStatement ps = null;
        InputStream is = null;
        try {
            //buat koneksi ke database
            String url = "jdbc:mysql://localhost:3306/belajar_gambar";
            String user = "root";
            String password = "";

            con = DriverManager.getConnection(url, user, password);
            ps = con.prepareCall("insert into gambar values (?,?)");
            ps.setInt(1, 101);
            is = new FileInputStream(new File("E:\\Kuliah MMI UGM\\Tesis\\Program Tesis (Java,yang update di laptop andi)\\project_gambar\\src\\main\\java\\com\\mycompany\\project_gambar\\image.jpg"));
            ps.setBinaryStream(2, is);
            int count = ps.executeUpdate();
            System.out.println("Count: "+count);
        }  catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally{
            try{
                if(is != null) is.close();
                if(ps != null) ps.close();
                if(con != null) con.close();
            } catch(Exception ex){}
        }
    }
}
